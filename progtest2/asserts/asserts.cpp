
#include <cassert>

int main() {

    {
        uint32_t id_leader;
        std::string name;

        CPulitzer bot(3, 10);
        assert(!bot.party_leader(1, id_leader)); // false
        assert(bot.register_politician(1, 5, "VK", 1000, 77)); // true
        assert(bot.register_politician(2, 4, "MZ", 1000, 77)); // true
        assert(bot.register_politician(2, 7, "VS", 500, 77)); // true
        assert(bot.party_leader(1, id_leader)); // true, 5
        assert(id_leader == 5);
        assert(bot.party_leader(2, id_leader)); // true, 4
        assert(id_leader == 4);
        assert(bot.change_popularity(7, 2000)); // true
        assert(bot.party_leader(2, id_leader)); // true, 7
        assert(id_leader == 7);
        assert(bot.register_politician(1, 2, "MT", 500, 77)); // true
        assert(!bot.register_politician(2, 2, "JP", 500, 77)); // false
        assert(bot.register_politician(2, 9, "JP", 500, 77)); // true
        assert(bot.deregister_politician(5)); // true
        assert(bot.party_leader(1, id_leader)); // true, 2
        assert(id_leader == 2);
        assert(bot.sack_leader(2)); // true
        assert(bot.change_popularity(9, 200)); // true
        assert(bot.sack_leader(1)); // true
    }
    {
        uint32_t id_leader;
        std::string name;

        CPulitzer bot(5, 5);
        assert(bot.register_politician(0, 0, "RS", 150, 77)); // true
        assert(bot.register_politician(1, 1, "RS", 50, 77)); // true
        assert(bot.register_politician(2, 2, "RS", 60, 77)); // true
        assert(bot.register_politician(3, 3, "VKml", 100, 77)); // true
        assert(bot.register_politician(3, 4, "ZMZ", 50, 70)); // true
        assert(bot.deregister_politician(3)); // true
        assert(bot.merge_parties(3, 2)); // true
        assert(bot.merge_parties(3, 1)); // true
        assert(bot.party_leader(0, id_leader)); // true, 0
        assert(id_leader == 0);
        assert(!bot.party_leader(1, id_leader)); // false
        assert(!bot.party_leader(2, id_leader)); // false
        assert(bot.party_leader(3, id_leader)); // true, 2
        assert(id_leader == 2);
    }

    {
        CPulitzer bot(100, 500);
        uint32_t id_leader;
        std::string name;
        assert(bot.register_politician(0, 1, "RS", 7150, 77));
        assert(bot.register_politician(0, 2, "RS", 6160, 77));
        assert(bot.register_politician(0, 3, "RS", 5170, 77));
        assert(bot.register_politician(0, 4, "RS", 4180, 77));
        assert(bot.register_politician(0, 5, "RS", 3190, 77));
        assert(bot.register_politician(0, 6, "RS", 2200, 77));
        assert(bot.register_politician(0, 7, "RS", 1210, 77));
        assert(bot.register_politician(0, 8, "RS", 1220, 77));
        assert(bot.register_politician(0, 9, "RS", 230, 77));
        assert(bot.register_politician(0, 10, "RS", 240, 77));
        assert(bot.register_politician(0, 11, "RS", 250, 77));
        assert(bot.register_politician(0, 12, "RS", 260, 77));
        assert(bot.register_politician(0, 13, "RS", 270, 77));
        assert(bot.register_politician(0, 14, "RS", 280, 77));
        assert(bot.register_politician(0, 15, "RS", 290, 77));
        assert(bot.register_politician(0, 16, "RS", 30000, 77));
        assert(bot.register_politician(0, 17, "RS", 150, 77));
        assert(bot.register_politician(0, 18, "RS", 200, 77));

        assert(bot.party_leader(0, id_leader) && id_leader == 16);
        assert(bot.change_popularity(4, 3000));
        assert(bot.party_leader(0, id_leader) && id_leader == 16);
        assert(bot.change_popularity(1, 40000));
        assert(bot.party_leader(0, id_leader) && id_leader == 1);
        assert(bot.change_popularity(2, 8000000));
        assert(bot.party_leader(0, id_leader));
        assert(id_leader == 2);
        assert(bot.change_popularity(1, 4000000000));
        assert(bot.party_leader(0, id_leader) && id_leader == 1);
    }

    {
        uint32_t id_leader;
        std::string name;
        CPulitzer bot(3, 10000);
        assert (!bot.party_leader(1, id_leader)); // false
        assert (bot.register_politician(1, 5, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 4, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 7, "VS", 500, 77)); // true
        assert (bot.register_politician(1, 51, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 41, "MZ", 300, 77)); // true
        assert (bot.register_politician(2, 71, "VS", 500, 77)); // true
        assert (bot.register_politician(1, 52, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 42, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 72, "VS", 500, 77)); // true
        assert (bot.register_politician(1, 53, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 43, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 73, "VS", 500, 77)); // true
        assert (bot.register_politician(1, 54, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 44, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 74, "VS", 500, 77)); // true
        assert (bot.register_politician(1, 55, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 45, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 75, "VS", 500, 77)); // true
        assert (bot.register_politician(1, 56, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 46, "MZ", 1000000000, 77)); // true
        assert (bot.register_politician(2, 76, "VS", 500, 77)); // true
        assert (bot.register_politician(1, 57, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 47, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 77, "VS", 5000, 77)); // true
        assert (bot.register_politician(1, 58, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 48, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 78, "VS", 5000, 77)); // true
        assert (bot.register_politician(1, 511, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 411, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 711, "VS", 500, 77)); // true
        assert (bot.register_politician(1, 521, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 421, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 721, "VS", 500000, 77)); // true
        assert (bot.register_politician(1, 531, "VK", 1000000, 77)); // true
        assert (bot.register_politician(2, 431, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 731, "VS", 500, 77)); // true
        assert (bot.register_politician(1, 541, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 441, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 741, "VS", 1000, 77)); // true
        assert (bot.register_politician(1, 551, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 451, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 751, "VS", 500, 77)); // true
        assert (bot.register_politician(1, 561, "VK", 10000000, 77)); // true
        assert (bot.register_politician(2, 461, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 761, "VS", 1000, 77)); // true
        assert (bot.register_politician(1, 571, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 471, "MZ", 1000, 77)); // true
        assert (bot.register_politician(2, 771, "VS", 1000, 77)); // true
        assert (bot.register_politician(1, 581, "VK", 1000, 77)); // true
        assert (bot.register_politician(2, 481, "MZ", 500, 77)); // true
        assert (bot.register_politician(2, 781, "VS", 10, 77)); // true

        assert(bot.party_leader(2, id_leader));
        assert (id_leader == 46);


        assert (bot.party_leader(1, id_leader) && id_leader == 561); // true, 561


        assert (bot.change_popularity(7, 2000000000)); // true
        assert (bot.party_leader(2, id_leader));
        assert (id_leader == 7); // true, 7


        assert (bot.register_politician(1, 2, "MT", 500, 77)); // true
        assert (!bot.register_politician(2, 2, "JP", 500, 77)); // false
        assert (bot.register_politician(2, 9, "JP", 500, 77)); // true
        assert (bot.deregister_politician(561)); // true
        assert (bot.party_leader(1, id_leader) && id_leader == 531); // true, 2



        CPulitzer bot1(5, 5);
        assert (bot1.register_politician(0, 0, "RS", 150, 77)); // true
        assert (bot1.register_politician(1, 1, "RS", 50, 77)); // true
        assert (bot1.register_politician(2, 2, "RS", 60, 77)); // true
        assert (bot1.register_politician(3, 3, "VKml", 100, 77)); // true
        assert (bot1.register_politician(3, 4, "ZMZ", 50, 70)); // true
        assert (bot1.deregister_politician(3)); // true
        assert (bot1.merge_parties(3, 2)); // true
        assert (bot1.merge_parties(3, 1)); // true
        assert (bot1.party_leader(0, id_leader) && id_leader == 0); // true, 0
        assert (!bot1.party_leader(1, id_leader)); // false
        assert (!bot1.party_leader(2, id_leader)); // false
        assert (bot1.party_leader(3, id_leader) && id_leader == 2); // true, 2


        CPulitzer bot6(100, 100000);
        assert (bot6.register_politician(1, 0, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 1, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 2, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 3, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 4, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 5, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 6, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 7, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 8, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 9, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 10, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 11, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 12, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 13, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 14, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 15, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 16, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 17, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 18, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 19, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 20, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 21, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 22, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 23, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 24, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 25, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 26, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 27, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 28, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 29, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 30, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 31, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 32, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 33, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 34, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 35, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 36, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 37, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 38, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 39, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 40, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 41, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 42, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 43, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 44, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 45, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 46, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 47, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 48, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 49, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 50, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 51, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 52, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 53, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 54, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 55, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 56, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 57, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 58, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 59, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 60, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 61, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 62, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 63, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 64, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 65, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 66, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 67, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 68, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 69, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 70, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 71, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 72, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 73, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 74, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 75, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 76, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 77, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 78, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 79, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 80, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 81, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 82, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 83, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 84, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 85, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 86, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 87, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 88, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 89, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 90, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 91, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 92, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 93, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 94, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 95, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 96, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 97, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 98, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 99, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 100, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 101, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 102, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 103, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 104, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 105, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 106, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 107, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 108, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 109, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 110, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 111, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 112, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 113, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 114, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 115, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 116, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 117, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 118, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 119, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 120, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 121, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 122, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 123, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 124, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 125, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 126, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 127, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 128, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 129, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 130, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 131, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 132, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 133, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 134, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 135, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 136, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 137, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 138, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 139, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 140, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 141, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 142, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 143, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 144, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 145, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 146, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 147, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 148, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 149, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 150, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 151, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 152, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 153, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 154, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 155, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 156, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 157, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 158, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 159, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 160, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 161, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 162, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 163, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 164, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 165, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 166, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 167, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 168, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 169, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 170, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 171, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 172, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 173, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 174, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 175, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 176, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 177, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 178, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 179, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 180, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 181, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 182, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 183, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 184, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 185, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 186, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 187, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 188, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 189, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 190, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 191, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 192, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 193, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 194, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(1, 195, "RS", 150, 77)); // true
        assert (bot6.register_politician(1, 196, "RS", 50, 77)); // true
        assert (bot6.register_politician(1, 197, "RS", 60, 77)); // true
        assert (bot6.register_politician(1, 198, "VKml", 100, 77)); // true
        assert (bot6.register_politician(1, 199, "ZMZ", 50, 70)); // true

        assert (bot6.register_politician(3, 200, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 201, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 202, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 203, "ZMZ", 50, 70)); // true











        assert (bot6.register_politician(3, 3010, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3011, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3012, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3013, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3014, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3015, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3016, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3017, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3018, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3019, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3020, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3021, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3022, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3023, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3024, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3025, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3026, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3027, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3028, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3029, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3030, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3031, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3032, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3033, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3034, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3035, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3036, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3037, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3038, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3039, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3040, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3041, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3042, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3043, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3044, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3045, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3046, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3047, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3048, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3049, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3050, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3051, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3052, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3053, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3054, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3055, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3056, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3057, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3058, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3059, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3060, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3061, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3062, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3063, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3064, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3065, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3066, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3067, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3068, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3069, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3070, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3071, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3072, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3073, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3074, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3075, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3076, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3077, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3078, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3079, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3080, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3081, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3082, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3083, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3084, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3085, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3086, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3087, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3088, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3089, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3090, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3091, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3092, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3093, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3094, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 3095, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 3096, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 3097, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 3098, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 3099, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30100, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30101, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30102, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30103, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30104, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30105, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30106, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30107, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30108, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30109, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30110, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30111, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30112, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30113, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30114, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30115, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30116, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30117, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30118, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30119, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30120, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30121, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30122, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30123, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30124, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30125, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30126, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30127, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30128, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30129, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30130, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30131, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30132, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30133, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30134, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30135, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30136, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30137, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30138, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30139, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30140, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30141, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30142, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30143, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30144, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30145, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30146, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30147, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30148, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30149, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30150, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30151, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30152, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30153, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30154, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30155, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30156, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30157, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30158, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30159, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30160, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30161, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30162, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30163, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30164, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30165, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30166, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30167, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30168, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30169, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30170, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30171, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30172, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30173, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30174, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30175, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30176, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30177, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30178, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30179, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30180, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30181, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30182, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30183, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30184, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30185, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30186, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30187, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30188, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30189, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30190, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30191, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30192, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30193, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30194, "ZMZ", 50, 70)); // true
        assert (bot6.register_politician(3, 30195, "RS", 150, 77)); // true
        assert (bot6.register_politician(3, 30196, "RS", 50, 77)); // true
        assert (bot6.register_politician(3, 30197, "RS", 60, 77)); // true
        assert (bot6.register_politician(3, 30198, "VKml", 100, 77)); // true
        assert (bot6.register_politician(3, 30199, "ZMZ", 50, 70)); // true





        assert(bot6.merge_parties(3, 1));

        assert(!bot6.party_leader(1, id_leader));
        assert(bot6.party_leader(3, id_leader) && id_leader == 30195);

        assert(!bot6.merge_parties(0, 1));
        assert(!bot6.merge_parties(0, 3));

        assert(bot6.register_politician(0, 301, "RS", 150, 77));
        assert(bot6.party_leader(0, id_leader) && id_leader == 301);

        assert(bot6.register_politician(0, 302, "RS", 150, 77));
        assert(bot6.party_leader(0, id_leader) && id_leader == 302);

        assert(bot6.register_politician(0, 303, "RS", 150, 77));
        assert(bot6.party_leader(0, id_leader) && id_leader == 303);

        assert(bot6.register_politician(0, 304, "RS", 150, 77));
        assert(bot6.party_leader(0, id_leader) && id_leader == 304);

        assert(bot6.sack_leader(0));
        assert(bot6.party_leader(0, id_leader) && id_leader == 303);

        assert(bot6.register_politician(0, 305, "RS", 200, 77));
        assert(bot6.party_leader(0, id_leader) && id_leader == 305);

        assert(bot6.register_politician(0, 306, "RS", 150, 77));
        assert(bot6.party_leader(0, id_leader) && id_leader == 305);

        assert(bot6.register_politician(0, 307, "RS", 200, 77));
        assert(bot6.party_leader(0, id_leader) && id_leader == 307);

        assert(bot6.change_popularity(305, 200));
        assert(bot6.party_leader(0, id_leader) && id_leader == 305);

        assert(!bot6.register_politician(0, 307, "RS", 150, 77));
        assert(bot6.party_leader(0, id_leader) && id_leader == 305);

        assert(bot6.sack_leader(0));
        assert(bot6.party_leader(0, id_leader) && id_leader == 307);

        assert(bot6.register_politician(0, 308, "RS", 200, 77));
        assert(bot6.party_leader(0, id_leader) && id_leader == 308);

        assert(bot6.merge_parties(0, 3));
        assert(!bot6.party_leader(3, id_leader));
        assert(bot6.party_leader(0, id_leader) && id_leader == 308);


        assert(!bot6.party_leader(3, id_leader));
        assert(!bot6.party_leader(1, id_leader));

        assert(bot6.register_politician(0, 4007, "RS", 20000, 77));
        assert(bot6.party_leader(0, id_leader) && id_leader == 4007);

        assert(bot6.merge_parties(0, 0));
        assert(bot6.party_leader(0, id_leader) && id_leader == 4007);
    }

    {
        uint32_t id_leader;
        CPulitzer bot(100, 1000);
        assert(bot.register_politician(0, 1, "RS", 20000, 77));
        assert(bot.register_politician(1, 2, "RS", 20000, 77));
        assert(bot.merge_parties(0, 1));
        assert(bot.party_leader(0, id_leader) && id_leader == 2);
        assert(!bot.party_leader(1, id_leader));
    }
    {
        uint32_t id_leader;
        CPulitzer bot(100, 1000);
        assert(bot.register_politician(0, 1, "RS", 20000, 77));
        assert(bot.register_politician(0, 2, "RS", 20000, 77));
        assert(bot.register_politician(1, 3, "RS", 20000, 77));
        assert(bot.merge_parties(0, 1));
        assert(bot.party_leader(0, id_leader) && id_leader == 3);
        assert(!bot.party_leader(1, id_leader));
    }
    {
        uint32_t id_leader;
        CPulitzer bot(100, 1000);
        assert(bot.register_politician(0, 1, "RS", 20000, 77));
        assert(bot.register_politician(0, 2, "RS", 20000, 77));
        assert(bot.register_politician(1, 3, "RS", 20000, 77));
        assert(bot.merge_parties(1, 0));
        assert(bot.party_leader(1, id_leader) && id_leader == 3);
        assert(!bot.party_leader(0, id_leader));
    }

    {
        uint32_t id_leader;
        CPulitzer bot(100, 1000);
        assert(bot.register_politician(0, 1, "RS", 20000, 77));
        assert(bot.register_politician(0, 2, "RS", 20000, 77));
        assert(bot.register_politician(1, 3, "RS", 20000, 77));
        assert(bot.merge_parties(1, 1));
        assert(bot.party_leader(1, id_leader) && id_leader == 3);
        assert(bot.party_leader(0, id_leader) && id_leader == 2);
    }

    {
        uint32_t id_leader;
        CPulitzer bot(100, 1000);
        assert(bot.register_politician(0, 1, "RS", 20000, 77));
        assert(!bot.merge_parties(0, 1));
        assert(!bot.merge_parties(1, 0));
        assert(bot.merge_parties(0, 0));
        assert(!bot.party_leader(1, id_leader));
        assert(bot.party_leader(0, id_leader) && id_leader == 1);
    }

    {
        uint32_t id_leader;
        CPulitzer bot(100, 1000);
        assert(bot.register_politician(0, 1, "RS", 20000, 77));
        assert(bot.register_politician(0, 2, "RS", 20000, 77));
        assert(bot.register_politician(1, 3, "RS", 20000, 77));
        assert(bot.register_politician(1, 4, "RS", 20000, 77));
        assert(bot.merge_parties(1, 0));
    }

    {
        uint32_t id_leader;
        CPulitzer bot(1000, 50000);
        assert(bot.register_politician(0, 0, "name", 20000, 77));
        assert(bot.sack_leader(0));
        assert(!bot.party_leader(0, id_leader));
    }

    {
        uint32_t id_leader;
        uint32_t pop;
        CPulitzer bot(1000, 50000);
        assert(bot.register_politician(0, 0, "name", 20000, 77));
        assert(bot.change_popularity(0, 150));
        assert(bot.sack_leader(0));
        assert(!bot.party_leader(0, id_leader));
        assert(!bot.politician_popularity(0, pop));
    }
    {
        uint32_t id_leader;
        CPulitzer bot(1000, 50000);
        assert(bot.register_politician(0, 0, "name", 20000, 77));
        assert(bot.register_politician(1, 1, "name", 20000, 77));
        assert(bot.merge_parties(0, 1));
        assert(bot.change_popularity(1, 200));
        assert(bot.politician_popularity(1, id_leader) && id_leader == 200);
        assert(bot.politician_popularity(0, id_leader) && id_leader == 20000);
        assert(!bot.party_leader(1, id_leader));
        assert(bot.party_leader(0, id_leader) && id_leader == 0);
        assert(bot.deregister_politician(0));
        assert(bot.party_leader(0, id_leader) && id_leader == 1);
    }
    {
        uint32_t id_leader;
        CPulitzer bot(1000, 50000);
        assert(bot.register_politician(0, 0, "name", 20000, 77));
        assert(bot.register_politician(1, 1, "name", 20000, 77));
        assert(bot.merge_parties(0, 1));
        assert(!bot.sack_leader(1));
        assert(bot.sack_leader(0));
        assert(bot.sack_leader(0));
        assert(!bot.party_leader(0, id_leader));
    }
    {
        uint32_t id_leader;
        CPulitzer bot(1000, 50000);
        assert(bot.register_politician(0, 0, "name", 20000, 77));
        assert(bot.register_politician(1, 1, "name", 20000, 77));
        assert(bot.merge_parties(0, 1));
        assert(bot.party_leader(0, id_leader) && id_leader == 1);
    }
    {
        uint32_t id_leader;
        CPulitzer bot ( 1000, 50000 );
        assert( bot.register_politician (0,0, "name",0, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 0);
        assert( bot.register_politician (1,1, "name",1, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 1);
        assert( bot.merge_parties(0,1));
        assert( bot.party_leader(0,id_leader));
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader));
        assert( bot.change_popularity(0, 500));
        assert( bot.deregister_politician(0));
    }
    {
        uint32_t id_leader;
        CPulitzer bot ( 1000, 50000 );
        assert( bot.register_politician (0,0, "name",0, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 0);
        assert( bot.register_politician (1,1, "name",1, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 1);
        assert( bot.register_politician (1,2, "name",2, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 2);
        assert( bot.merge_parties(0,1));
        assert( bot.party_leader(0,id_leader));
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader));
        assert( bot.change_popularity(0, 500));
        assert( bot.deregister_politician(0));
    }
    {
        uint32_t id_leader;
        CPulitzer bot ( 1000, 50000 );
        assert( bot.register_politician (0,0, "name",0, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 0);
        assert( bot.register_politician (0,1, "name",1, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 1);
        assert( bot.register_politician (1,2, "name",2, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 2);
        assert( bot.register_politician (1,3, "name",3, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 3);
        assert( bot.merge_parties(0,1));
        assert( bot.party_leader(0,id_leader));
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader));
        assert( bot.change_popularity(0, 500));
        assert( bot.deregister_politician(0));
    }
    {
        uint32_t id_leader;
        CPulitzer bot ( 1000, 50000 );
        assert( bot.register_politician (0,0, "name",0, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 0);
        assert( bot.register_politician (0,1, "name",1, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 1);
        assert( bot.register_politician (1,2, "name",2, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 2);
        assert( bot.register_politician (1,3, "name",3, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 3);
        assert( bot.register_politician (1,4, "name",4, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 4);
        assert( bot.merge_parties(0,1));
        assert( bot.party_leader(0,id_leader));
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader));
        assert( bot.change_popularity(0, 500));
        assert( bot.deregister_politician(0));
    }
    {
        uint32_t id_leader;
        CPulitzer bot ( 1000, 50000 );
        assert( bot.register_politician (0,0, "name",0, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 0);
        assert( bot.register_politician (0,1, "name",1, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 1);
        assert( bot.register_politician (0,2, "name",2, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 2);
        assert( bot.register_politician (1,3, "name",3, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 3);
        assert( bot.register_politician (1,4, "name",4, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 4);
        assert( bot.register_politician (1,5, "name",5, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 5);
        assert( bot.merge_parties(0,1));
        assert( bot.party_leader(0,id_leader));
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader));
        assert( bot.change_popularity(0, 500));
        assert( bot.deregister_politician(0));
    }
    {
        uint32_t id_leader;
        CPulitzer bot ( 1000, 50000 );
        assert( bot.register_politician (0,0, "name",0, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 0);
        assert( bot.register_politician (0,1, "name",1, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 1);
        assert( bot.register_politician (0,2, "name",2, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 2);
        assert( bot.register_politician (1,3, "name",3, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 3);
        assert( bot.register_politician (1,4, "name",4, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 4);
        assert( bot.register_politician (1,5, "name",5, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 5);
        assert( bot.register_politician (1,6, "name",6, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 6);
        assert( bot.merge_parties(0,1));
        assert( bot.party_leader(0,id_leader));
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader));
        assert( bot.change_popularity(0, 500));
        assert( bot.deregister_politician(0));
    }
    {
        uint32_t id_leader;
        CPulitzer bot ( 1000, 50000 );
        assert( bot.register_politician (0,0, "name",0, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 0);
        assert( bot.register_politician (0,1, "name",1, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 1);
        assert( bot.register_politician (0,2, "name",2, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 2);
        assert( bot.register_politician (0,3, "name",3, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 3);
        assert( bot.register_politician (1,4, "name",4, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 4);
        assert( bot.register_politician (1,5, "name",5, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 5);
        assert( bot.register_politician (1,6, "name",6, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 6);
        assert( bot.register_politician (1,7, "name",7, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 7);
        assert( bot.merge_parties(0,1));
        assert( bot.party_leader(0,id_leader));
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader));
        assert( bot.change_popularity(0, 500));
        assert( bot.deregister_politician(0));
    }
    {
        uint32_t id_leader;
        CPulitzer bot ( 1000, 50000 );
        assert( bot.register_politician (0,0, "name",0, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 0);
        assert( bot.register_politician (0,1, "name",1, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 1);
        assert( bot.register_politician (0,2, "name",2, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 2);
        assert( bot.register_politician (0,3, "name",3, 77 ));
        assert( bot.party_leader(0, id_leader) && id_leader == 3);
        assert( bot.register_politician (1,4, "name",4, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 4);
        assert( bot.register_politician (1,5, "name",5, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 5);
        assert( bot.register_politician (1,6, "name",6, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 6);
        assert( bot.register_politician (1,7, "name",7, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 7);
        assert( bot.register_politician (1,8, "name",8, 77 ));
        assert( bot.party_leader(1, id_leader) && id_leader == 8);
        assert( bot.merge_parties(0,1));
        assert( bot.party_leader(0,id_leader) && id_leader == 8);
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader) && id_leader == 7);
        assert( bot.change_popularity(0, 500));
        assert( bot.party_leader(0,id_leader) && id_leader == 0);
        assert( bot.deregister_politician(0));
        assert( bot.party_leader(0,id_leader) && id_leader == 7);
        assert( bot.change_popularity(6, 7));
        assert( bot.party_leader(0,id_leader) && id_leader == 6);
        assert( bot.change_popularity(7, 7));
        assert( bot.party_leader(0,id_leader) && id_leader == 7);

        assert( bot.register_politician (1,10, "name",7, 77 ));
        assert( bot.register_politician (1,11, "name",7, 77 ));
        assert( bot.register_politician (1,12, "name",7, 77 ));
        assert( bot.register_politician (1,13, "name",7, 77 ));

        assert( bot.merge_parties(0,1));

        assert( ! bot.sack_leader(1));

        assert( bot.party_leader(0,id_leader) && id_leader == 13);
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader) && id_leader == 12);
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader) && id_leader == 11);
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader) && id_leader == 10);
        assert( bot.sack_leader(0));
        assert( bot.party_leader(0,id_leader) && id_leader == 7);

        assert( bot.register_politician (1,10, "name",7, 77 ));
        assert( bot.register_politician (1,11, "name",7, 77 ));
        assert( bot.register_politician (1,12, "name",7, 77 ));
        assert( bot.register_politician (1,13, "name",7, 77 ));
        assert( bot.merge_parties( 0, 1));
        assert( bot.party_leader(0,id_leader) && id_leader == 13);
        assert( bot.change_popularity(7, 7));
        assert( bot.party_leader(0,id_leader) && id_leader == 7);
        assert( bot.change_popularity(13, 7));
        assert( !bot.party_leader(1, id_leader));
        assert( bot.party_leader(0,id_leader) && id_leader == 13);

    }


    return 0;
}
