#include <iostream>
#include <utility>

//----------------------------------------------------------------------------------------------------------------------
class CPolitician;

//----------------------------------------------------------------------------------------------------------------------
class Node {
public:
    explicit Node(CPolitician *politician)
            : politician(politician),
              degree(0),
              parent(nullptr),
              sibling(nullptr),
              child(nullptr) {
    }

    CPolitician *politician;
    int degree;
    Node *parent;
    Node *sibling;
    Node *child;
};

//----------------------------------------------------------------------------------------------------------------------
class CPolitician {
public:
    //------------------------------------------------------------------------------------------------------------------
    CPolitician(uint32_t id_party, uint32_t id_politician, std::string name, uint32_t popularity,
                uint8_t gender, uint64_t counter)
            : mID(id_politician),
              mPartyID(id_party),
              mGender(gender),
              mPopularity(popularity),
              mName(std::move(name)),
              lastPopChangeTime(counter) {}
    //------------------------------------------------------------------------------------------------------------------
    ~CPolitician() {
        delete nodePtr;
    }
    //------------------------------------------------------------------------------------------------------------------
public:
    uint32_t mID{};
    uint32_t mPartyID{};
    uint8_t mGender{};
    uint32_t mPopularity{};
    std::string mName;
    Node *nodePtr{};
    uint64_t lastPopChangeTime;
};
//----------------------------------------------------------------------------------------------------------------------

class CLinkedList {
public:
    //------------------------------------------------------------------------------------------------------------------
    void pushback(Node *node) {
        if (root == nullptr) {
            root = node;
            root->sibling = nullptr;
            return;
        }
        Node *tmp = root->sibling;
        Node *prev = root;
        while (tmp) {
            prev = tmp;
            tmp = tmp->sibling;
        }
        prev->sibling = node;
        prev->sibling->sibling = nullptr;
    }

    //------------------------------------------------------------------------------------------------------------------
    Node *findMax() const {
        if (!root)
            return nullptr;

        Node *tmp = root;
        uint32_t max = tmp->politician->mPopularity;
        Node *maxPtr = tmp;
        tmp = tmp->sibling;
        while (tmp) {
            if (tmp->politician->mPopularity > max || (tmp->politician->mPopularity == max &&
                                                       tmp->politician->lastPopChangeTime >
                                                       maxPtr->politician->lastPopChangeTime)) {
                maxPtr = tmp;
                max = tmp->politician->mPopularity;
            }
            tmp = tmp->sibling;
        }
        return maxPtr;
    }

    //------------------------------------------------------------------------------------------------------------------
    Node *findMax(Node *&prev) const {
        if (!root)
            return nullptr;

        Node *tmp = root;
        uint32_t max = tmp->politician->mPopularity;
        Node *maxPtr = tmp;
        prev = nullptr;
        while (tmp) {
            if (tmp->politician->mPopularity > max || (tmp->politician->mPopularity == max &&
                                                       tmp->politician->lastPopChangeTime >
                                                       maxPtr->politician->lastPopChangeTime)) {
                maxPtr = tmp;
                max = tmp->politician->mPopularity;
            }
            if (tmp->sibling &&
                (tmp->sibling->politician->mPopularity > max || (tmp->sibling->politician->mPopularity == max &&
                                                                 tmp->sibling->politician->lastPopChangeTime >
                                                                 maxPtr->politician->lastPopChangeTime))) {
                prev = tmp;
            }
            tmp = tmp->sibling;
        }
        return maxPtr;
    }

    //------------------------------------------------------------------------------------------------------------------
    void pushFront(Node *node) {
        Node *tmp = root;
        root = node;
        root->sibling = tmp;
    }

    //------------------------------------------------------------------------------------------------------------------
    Node *root{};
};

//----------------------------------------------------------------------------------------------------------------------

Node *BHMergeTrees(Node *a, Node *b) {
    Node *result;
    if (a->politician->mPopularity > b->politician->mPopularity ||
        (a->politician->mPopularity == b->politician->mPopularity &&
         a->politician->lastPopChangeTime > b->politician->lastPopChangeTime)) {
        result = a;
        b->parent = a;
        b->sibling = a->child;
        a->child = b;
    } else {
        result = b;
        a->parent = b;
        a->sibling = b->child;
        b->child = a;
    }
    result->degree = a->degree + 1;
    return result;
}

//----------------------------------------------------------------------------------------------------------------------

Node *BHFindMax(CLinkedList heap) {
    return heap.findMax();
}

//----------------------------------------------------------------------------------------------------------------------

CLinkedList BHMerge(CLinkedList heap1, CLinkedList heap2) {
    if (!heap1.root)
        return heap2;
    if (!heap2.root)
        return heap1;

    CLinkedList result;
    Node *scitanci[3]{};
    Node *carry = nullptr;
    int neprazdnych = 2;
    int akt_rad = 0;
    while (neprazdnych >= 2) {
        neprazdnych = 0;
        int pocet = 0;
        if (heap1.root) {
            neprazdnych++;
            Node *a = heap1.root;
            if (a->degree == akt_rad) {
                scitanci[pocet] = a;
                pocet++;
                heap1.root = heap1.root->sibling;
            }
        }
        if (heap2.root) {
            neprazdnych++;
            Node *b = heap2.root;
            if (b->degree == akt_rad) {
                scitanci[pocet] = b;
                pocet++;
                heap2.root = heap2.root->sibling;
            }
        }
        if (carry) {
            neprazdnych++;
            scitanci[pocet] = carry;
            pocet++;
            carry = nullptr;
        }

        for (int i = 0; i < pocet; ++i)
            scitanci[i]->degree = akt_rad;

        if (pocet == 3) {
            result.pushback(scitanci[2]);
            carry = BHMergeTrees(scitanci[0], scitanci[1]);
        }
        if (pocet == 2)
            carry = BHMergeTrees(scitanci[0], scitanci[1]);
        if (pocet == 1) {
            result.pushback(scitanci[0]);
        }
        akt_rad++;
    }

    Node *res;
    if (heap1.root) {
        res = result.root;
        while (res->sibling)
            res = res->sibling;
        res->sibling = heap1.root;
    }

    if (heap2.root) {
        res = result.root;
        while (res->sibling)
            res = res->sibling;
        res->sibling = heap2.root;
    }

    return result;
}

//----------------------------------------------------------------------------------------------------------------------

CLinkedList BHInsert(CLinkedList heap, CPolitician *politician) {
    CLinkedList tempHeap;
    auto node = new Node(politician);
    politician->nodePtr = node;
    tempHeap.pushback(node);
    return BHMerge(heap, tempHeap);
}

//----------------------------------------------------------------------------------------------------------------------

CLinkedList removeRootFromTree(Node *root) {
    CLinkedList heap;
    Node *tmp = root->child;
    Node *lo;

    while (tmp) {
        lo = tmp;
        tmp = tmp->sibling;
        lo->sibling = nullptr;
        lo->parent = nullptr;
        heap.pushFront(lo);
    }
    return heap;
}

//----------------------------------------------------------------------------------------------------------------------

CLinkedList BHExtractMax(CLinkedList heap) {
    Node *prev = nullptr, *next, *max;
    max = heap.findMax(prev);
    next = max->sibling;

    if (prev == nullptr && next == nullptr)
        return removeRootFromTree(max);
    if (next == nullptr)
        prev->sibling = nullptr;

    else if (prev == nullptr)
        heap.root = next;
    else
        prev->sibling = next;

    return BHMerge(heap, removeRootFromTree(max));
}

//----------------------------------------------------------------------------------------------------------------------

void swap(Node *n1, Node *n2) {
    auto tmp = n1->politician;
    n1->politician = n2->politician;
    n2->politician = tmp;
    n1->politician->nodePtr = n1;
    n2->politician->nodePtr = n2;
}

//----------------------------------------------------------------------------------------------------------------------

void BHBubbleUp(Node *node) {
    while (node->parent) {
        if (node->parent->politician->mPopularity > node->politician->mPopularity ||
            (node->parent->politician->mPopularity == node->politician->mPopularity &&
             node->parent->politician->lastPopChangeTime > node->politician->lastPopChangeTime))
            return;
        swap(node->parent, node);
        node = node->parent;
    }
}
//----------------------------------------------------------------------------------------------------------------------

void updatePartyIDTree(Node *node, uint32_t party_id) {
    while (node) {
        node->politician->mPartyID = party_id;
        updatePartyIDTree(node->child, party_id);
        node = node->sibling;
    }
}

//----------------------------------------------------------------------------------------------------------------------

class CParty {
public:
    explicit CParty(uint32_t id_party)
            : mID(id_party) {}

    void insertIntoParty(CPolitician *politician) {
        mHeapOfMembers = BHInsert(mHeapOfMembers, politician);
    }

    //------------------------------------------------------------------------------------------------------------------
    CPolitician *findLeader() const {
        return (BHFindMax(mHeapOfMembers))->politician;
    }

    //------------------------------------------------------------------------------------------------------------------
    void removeLeader() {
        mHeapOfMembers = BHExtractMax(mHeapOfMembers);
    }

    //------------------------------------------------------------------------------------------------------------------
    void merge(CParty *party) {
        mHeapOfMembers = BHMerge(mHeapOfMembers, party->mHeapOfMembers);
    }

    //------------------------------------------------------------------------------------------------------------------
    void updatePartyID(uint32_t id_party) {
        mID = id_party;
        auto tmp = mHeapOfMembers.root;
        while (tmp) {
            updatePartyIDTree(tmp, id_party);
            tmp = tmp->sibling;
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    uint32_t mID;
    CLinkedList mHeapOfMembers;
};

//----------------------------------------------------------------------------------------------------------------------
class CPulitzer {
public:
    CPulitzer(size_t P, size_t N)
            : mMaxPoliticians(N),
              mMaxParties(P),
              counter(0) {
        mPoliticians = new CPolitician *[mMaxPoliticians];
        for (size_t i = 0; i < mMaxPoliticians; ++i)
            mPoliticians[i] = nullptr;
        mParties = new CParty *[mMaxParties];
        for (size_t i = 0; i < mMaxParties; ++i)
            mParties[i] = nullptr;
    }

    //------------------------------------------------------------------------------------------------------------------
    ~CPulitzer() {
        for (size_t i = 0; i < mMaxPoliticians; ++i) {
            delete mPoliticians[i];
        }
        for (size_t i = 0; i < mMaxParties; ++i) {
            delete mParties[i];
        }
        delete[] mPoliticians;
        delete[] mParties;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool register_politician(uint32_t id_party, uint32_t id_politician, const std::string &name, uint32_t popularity,
                             uint8_t gender) {
        if (id_politician >= mMaxPoliticians || id_politician < 0 || id_party < 0 || id_party >= mMaxParties ||
            mPoliticians[id_politician])
            return false;
        mPoliticians[id_politician] = new CPolitician(id_party, id_politician, name, popularity, gender, counter);
        counter++;

        if (!mParties[id_party]) {
            mParties[id_party] = new CParty(id_party);
        }
        mParties[id_party]->insertIntoParty(mPoliticians[id_politician]);
        return true;

    }

    //------------------------------------------------------------------------------------------------------------------
    bool politician_name(uint32_t id_politician, std::string &name) const {
        if (id_politician >= mMaxPoliticians || id_politician < 0 || !mPoliticians[id_politician])
            return false;
        name = mPoliticians[id_politician]->mName;
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool politician_gender(uint32_t id_politician, uint8_t &gender) const {
        if (id_politician >= mMaxPoliticians || id_politician < 0 || !mPoliticians[id_politician])
            return false;
        gender = mPoliticians[id_politician]->mGender;
        return true;
    }

    uint32_t politician_partyID(uint32_t id_politician) const {
        return mPoliticians[id_politician]->mPartyID;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool politician_popularity(uint32_t id_politician, uint32_t &popularity) const {
        if (id_politician >= mMaxPoliticians || id_politician < 0 || !mPoliticians[id_politician])
            return false;
        popularity = mPoliticians[id_politician]->mPopularity;
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool deregister_politician(uint32_t id_politician) {
        if (id_politician >= mMaxPoliticians || id_politician < 0 || !mPoliticians[id_politician])
            return false;

        change_popularity(id_politician, -1);
        sack_leader(mPoliticians[id_politician]->mPartyID);
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool party_leader(uint32_t id_party, uint32_t &id_leader) const {
        if (id_party >= mMaxParties || id_party < 0 || !mParties[id_party])
            return false;
        id_leader = mParties[id_party]->findLeader()->mID;
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool change_popularity(uint32_t id_politician, uint32_t popularity) {
        if (id_politician >= mMaxPoliticians || id_politician < 0 || !mPoliticians[id_politician])
            return false;

        if (mPoliticians[id_politician]->mPopularity <= popularity) {
            mPoliticians[id_politician]->mPopularity = popularity;
            mPoliticians[id_politician]->lastPopChangeTime = counter;
            BHBubbleUp(mPoliticians[id_politician]->nodePtr);
            counter++;
            return true;
        } else {
            CPolitician tmp(mPoliticians[id_politician]->mPartyID,
                            mPoliticians[id_politician]->mID,
                            mPoliticians[id_politician]->mName,
                            popularity,
                            mPoliticians[id_politician]->mGender,
                            counter);

            deregister_politician(id_politician);
            register_politician(tmp.mPartyID, tmp.mID, tmp.mName, popularity, tmp.mGender);
        }
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool sack_leader(uint32_t id_party) {
        if (id_party >= mMaxPoliticians || id_party < 0 || !mParties[id_party]) {
            return false;
        }

        uint32_t id = mParties[id_party]->findLeader()->mID;
        mParties[id_party]->removeLeader();

        if (!mParties[id_party]->mHeapOfMembers.root) {
            delete mParties[id_party];
            mParties[id_party] = nullptr;
        }

        delete mPoliticians[id];
        mPoliticians[id] = nullptr;
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool merge_parties(uint32_t dest_party, uint32_t src_party) {
        if (dest_party >= mMaxParties || src_party >= mMaxParties || dest_party < 0 || src_party < 0 ||
            !mParties[src_party] || !mParties[dest_party])
            return false;
        if (dest_party == src_party)
            return true;

        mParties[src_party]->updatePartyID(dest_party);
        mParties[dest_party]->merge(mParties[src_party]);

        delete mParties[src_party];
        mParties[src_party] = nullptr;
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool create_coalition(uint32_t id_party1, uint32_t id_party2) {
        if (id_party2 >= mMaxParties || id_party1 >= mMaxParties || id_party2 < 0 || id_party1 < 0 ||
            !mParties[id_party1] || !mParties[id_party2] ||
            id_party1 == id_party2)
            return false;
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool leave_coalition(uint32_t id_party) {
        if (id_party >= mMaxParties || id_party < 0 || !mParties[id_party])
            return false;
        return false;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool coalition_leader(uint32_t id_party, uint32_t &id_leader) const {

        if (id_party >= mMaxParties || id_party < 0 || !mParties[id_party])
            return false;

        id_leader = mParties[id_party]->findLeader()->mID;
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool scandal_occured(uint32_t id_party) {
        if (id_party >= mMaxParties || id_party < 0 || !mParties[id_party])
            return false;
        sack_leader(id_party);
        return true;
    }
    //------------------------------------------------------------------------------------------------------------------
private:
    size_t mMaxPoliticians;
    size_t mMaxParties;
    CPolitician **mPoliticians;
    CParty **mParties;
    uint64_t counter;
};

//----------------------------------------------------------------------------------------------------------------------
int main() {
    return 0;
}