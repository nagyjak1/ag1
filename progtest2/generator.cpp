#include <iostream>

using namespace std;

int main ( void ) {

    int max = 10;
    for ( int j = 2; j < max; ++j ) {
        int party = 0;
        cout << "{" << endl;
        cout << "uint32_t id_leader;" << endl;
        cout << "CPulitzer bot ( 1000, 50000 ); " << endl;
        for (int i = 0; i < j; ++i) {
            if (i == j / 2)
                party++;
            cout << "assert( bot.register_politician (" << party << "," << i << ", \"name\"," << i << ", 77 ));" << endl;
            cout << "assert( bot.party_leader(" << party << ", id_leader) && id_leader == " << i << ");" << endl;
        }
        cout << "assert( bot.merge_parties(0,1));" << endl;
        cout << "assert( bot.party_leader(0,id_leader));" << endl;
        cout << "assert( bot.sack_leader(0));" << endl;
        cout << "assert( bot.party_leader(0,id_leader));" << endl;
        cout << "assert( bot.change_popularity(0, 500));" << endl;
        cout << "assert( bot.deregister_politician(0));" << endl;
        cout << "}" << endl;
    }
    return 0;
}