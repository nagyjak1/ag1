#include <iostream>
#include <cmath>

const int intmax = 2147483647;

using namespace std;

class CLinkedListNode {
public:
    CLinkedListNode *next;
    int data;

    explicit CLinkedListNode(int i)
            : next(nullptr),
              data(i) {}
};

//----------------------------------------------------------------------------------------------------------

class CLinkedList {
public:
    CLinkedListNode *head;

    CLinkedList()
            : head(nullptr) {}

    ~CLinkedList() {
        auto tmp = head;
        while (tmp != nullptr) {
            auto x = tmp;
            tmp = tmp->next;
            delete x;
        }
    }

    void add(int i) {
        auto x = head;
        auto *tmp = new CLinkedListNode(i);
        if (head == nullptr)
            head = tmp;
        else {
            while (x->next != nullptr)
                x = x->next;
            x->next = tmp;
        }
    }
};

//----------------------------------------------------------------------------------------------------------

class CLever {
public:
    int pos;
    bool *changeVector;

    CLever()
            : pos(-1),
              changeVector(nullptr) {
    }

    ~CLever() {
        delete[] changeVector;
    }

    void allocChangeVector(int mapSize) {
        changeVector = new bool[mapSize];
    }
};

//----------------------------------------------------------------------------------------------------------

class CCord {
public:
    CCord(int x, int y) :
            m_x(x),
            m_y(y) {}

    int m_x;
    int m_y;
};

//----------------------------------------------------------------------------------------------------------

class QueueNode {
public:
    explicit QueueNode(CCord x) :
            data(x),
            next(nullptr) {}

    CCord data;
    QueueNode *next;
};

//----------------------------------------------------------------------------------------------------------

class CQueue {
public:
    CQueue() {
        front = rear = nullptr;
    }

    ~CQueue() {
        while (front)
            pop();
    }

    QueueNode *getFront() {
        if (!front)
            return nullptr;
        return front;
    }

    void pop() {
        if (front == nullptr)
            return;
        QueueNode *tmp = front;
        front = front->next;

        if (front == nullptr)
            rear = nullptr;

        delete tmp;
    }

    void push(CCord x) {
        auto *tmp = new QueueNode(x);

        if (front == nullptr) {
            front = tmp;
            rear = tmp;
            return;
        }
        rear->next = tmp;
        rear = tmp;
    }

    bool empty() {
        return front == nullptr;
    }

private:
    QueueNode *front;
    QueueNode *rear;
};

//----------------------------------------------------------------------------------------------------------

class CNode {
public:
    CNode()
            : m_Visited(false),
              m_Passable(false),
              m_Source(-1, -1),
              m_isInQueue(false),
              m_timeVisited(0),
              m_OriginalValue(false) {}

    bool canVisit() const {
        return m_Passable && !m_Visited && !m_isInQueue;
    }

    void restoreOriginal() {
        m_Passable = m_OriginalValue;
        m_Visited = false;
        m_isInQueue = false;
        m_timeVisited = 0;
        m_Source = CCord(-1, -1);
    }

    bool m_Visited;
    bool m_Passable;
    CCord m_Source;
    bool m_isInQueue;
    int m_timeVisited;
    bool m_OriginalValue;
};

//----------------------------------------------------------------------------------------------------------

void loadInput(CNode **&map, int &n, int &k, CCord &trophy, CLever *&levers) {
    for (int i = 0; i < k; ++i) {
        char c;
        cin >> levers[i].pos;
        levers[i].allocChangeVector(n);
        for (int l = 0; l < n; ++l) {
            cin >> c;
            c == '1' ? levers[i].changeVector[l] = true : levers[i].changeVector[l] = false;
        }
    }

    for (int i = 0; i < n; ++i) {
        for (int l = 0; l < n; ++l) {
            char tmp;
            cin >> tmp;
            tmp == '1' ? map[l][i].m_Passable = false : map[l][i].m_Passable = true;
            map[l][i].m_OriginalValue = map[l][i].m_Passable;
        }
    }
    cin >> trophy.m_x >> trophy.m_y;
    --trophy.m_x;
    --trophy.m_y;
}

//----------------------------------------------------------------------------------------------------------

void bfs(CNode **&map, int n, CCord &start) {
    CQueue q;
    q.push(start);

    if (!map[start.m_x][start.m_y].m_Passable)
        return;

    map[start.m_x][start.m_y].m_Visited = true;
    map[start.m_x][start.m_y].m_isInQueue = true;

    while (!q.empty()) {
        CCord tmp = q.getFront()->data;

        if (map[0][0].m_Visited)
            return;

        map[tmp.m_x][tmp.m_y].m_Visited = true;

        // check space on the right
        if (tmp.m_x != n - 1 && map[tmp.m_x + 1][tmp.m_y].canVisit()) {
            map[tmp.m_x + 1][tmp.m_y].m_Source = tmp;
            map[tmp.m_x + 1][tmp.m_y].m_timeVisited = map[tmp.m_x][tmp.m_y].m_timeVisited + 1;
            map[tmp.m_x + 1][tmp.m_y].m_isInQueue = true;
            q.push(CCord(tmp.m_x + 1, tmp.m_y));
        }

        // check space on the left
        if (tmp.m_x != 0 && map[tmp.m_x - 1][tmp.m_y].canVisit()) {
            map[tmp.m_x - 1][tmp.m_y].m_Source = tmp;
            map[tmp.m_x - 1][tmp.m_y].m_timeVisited = map[tmp.m_x][tmp.m_y].m_timeVisited + 1;
            map[tmp.m_x - 1][tmp.m_y].m_isInQueue = true;
            q.push(CCord(tmp.m_x - 1, tmp.m_y));
        }

        // check space above
        if (tmp.m_y != n - 1 && map[tmp.m_x][tmp.m_y + 1].canVisit()) {
            map[tmp.m_x][tmp.m_y + 1].m_Source = tmp;
            map[tmp.m_x][tmp.m_y + 1].m_timeVisited = map[tmp.m_x][tmp.m_y].m_timeVisited + 1;
            map[tmp.m_x][tmp.m_y + 1].m_isInQueue = true;
            q.push(CCord(tmp.m_x, tmp.m_y + 1));
        }

        // check space under
        if (tmp.m_y != 0 && map[tmp.m_x][tmp.m_y - 1].canVisit()) {
            map[tmp.m_x][tmp.m_y - 1].m_Source = tmp;
            map[tmp.m_x][tmp.m_y - 1].m_timeVisited = map[tmp.m_x][tmp.m_y].m_timeVisited + 1;
            map[tmp.m_x][tmp.m_y - 1].m_isInQueue = true;
            q.push(CCord(tmp.m_x, tmp.m_y - 1));
        }
        q.pop();
    }
}

//----------------------------------------------------------------------------------------------------------

void applyChanges(CNode **&map, CLever *&levers, CLinkedList &leversIndexes, int &mapSize) {
    bool *mapChanger = new bool[mapSize];
    auto tmp = leversIndexes.head;

    if (leversIndexes.head) {
        for (int i = 0; i < mapSize; ++i)
            mapChanger[i] = levers[tmp->data].changeVector[i];
        tmp = tmp->next;

        while (tmp != nullptr) {
            for (int l = 0; l < mapSize; ++l)
                mapChanger[l] ^= levers[tmp->data].changeVector[l];
            tmp = tmp->next;
        }
        for (int i = 0; i < mapSize; ++i)
            for (int j = 0; j < mapSize; ++j)
                map[j][i].m_Passable ^= mapChanger[j];
    }
    delete[] mapChanger;
}

//----------------------------------------------------------------------------------------------------------

int countStepsLevers(CLever *&levers, CLinkedList &leversIndexes) {
    int x = 0, max = 0;
    auto tmp = leversIndexes.head;

    if (tmp == nullptr)
        return 0;

    if (levers[tmp->data].pos == -1)
        x += 2;
    else
        max = levers[tmp->data].pos;
    tmp = tmp->next;
    while (tmp != nullptr) {
        if (levers[tmp->data].pos == -1)
            x += 2;
        else {
            if (levers[tmp->data].pos > max)
                max = levers[tmp->data].pos;
        }
        tmp = tmp->next;
    }
    return 2 * max + x;
}

//----------------------------------------------------------------------------------------------------------
// inspired by https://www.geeksforgeeks.org/power-set/
void generatePowerSet(int setSize, int powerSetSize, CLinkedList *powerSet) {
    int *set = new int[setSize];
    for (int i = 0; i < setSize; ++i)
        set[i] = i;

    for (int counter = 0; counter < powerSetSize; counter++) {
        for (int j = 0; j < setSize; j++) {
            if (counter & (1 << j))
                powerSet[counter].add(set[j]);
        }
    }
    delete[] set;
}

//----------------------------------------------------------------------------------------------------------

void restoreMap(CNode **map, int mapSize) {
    for (int i = 0; i < mapSize; ++i)
        for (int l = 0; l < mapSize; ++l)
            map[l][i].restoreOriginal();
}

//----------------------------------------------------------------------------------------------------------

void printPath(CNode **&map, CCord &trophy) {
    CCord start(0, 0);
    while (!(start.m_x == trophy.m_x && start.m_y == trophy.m_y)) {
        cout << "[" << start.m_x + 1 << ";" << start.m_y + 1 << "],";
        CCord tmp = start;
        start.m_x = map[tmp.m_x][tmp.m_y].m_Source.m_x;
        start.m_y = map[tmp.m_x][tmp.m_y].m_Source.m_y;
    }
    cout << "[" << start.m_x + 1 << ";" << start.m_y + 1 << "]\n";
}

//----------------------------------------------------------------------------------------------------------

void
findBestSolution(CNode **map, int mapSize, CCord trophy, CLever *levers, int powerSetSize, CLinkedList *powerSetList,
                 int &minSteps, int &bestChoiceIndex) {
    for (int i = 0; i < powerSetSize; ++i) {
        applyChanges(map, levers, powerSetList[i], mapSize);
        bfs(map, mapSize, trophy);

        if (map[0][0].m_Visited) {
            int x = map[0][0].m_timeVisited + countStepsLevers(levers, powerSetList[i]);
            if (x < minSteps) {
                minSteps = x;
                bestChoiceIndex = i;
            }
        }
        restoreMap(map, mapSize);
    }
}

//----------------------------------------------------------------------------------------------------------

void printOutput(int minSteps, int bestChoiceIndex, CNode **map, CLever *levers, int mapSize, CCord trophy,
                 CLinkedList *powerSetList, int leversNmb) {
    if (minSteps == intmax)
        cout << "-1\n";

    else {
        applyChanges(map, levers, powerSetList[bestChoiceIndex], mapSize);
        bfs(map, mapSize, trophy);
        cout << map[0][0].m_timeVisited + countStepsLevers(levers, powerSetList[bestChoiceIndex]) << "\n";

        auto tmp = powerSetList[bestChoiceIndex].head;

        for (int i = 0; i < leversNmb; i++) {
            if (tmp && i == tmp->data) {
                cout << 1;
                tmp = tmp->next;
            } else
                cout << 0;
            if (i == leversNmb - 1)
                cout << "\n";
        }
        printPath(map, trophy);
    }
}

//----------------------------------------------------------------------------------------------------------

int main() {
    int n, k;
    cin >> n >> k;
    CCord trophy(-1, -1);

    auto *levers = new CLever[k];
    auto **map = new CNode *[n];
    for (int i = 0; i < n; ++i)
        map[i] = new CNode[n];

    loadInput(map, n, k, trophy, levers);

    int powerSetSize = pow(2, k);
    auto *powerSetList = new CLinkedList[powerSetSize];
    generatePowerSet(k, powerSetSize, powerSetList);

    int bestChoiceIndex = 0, minSteps = intmax;
    findBestSolution(map, n, trophy, levers, powerSetSize, powerSetList, minSteps, bestChoiceIndex);
    printOutput(minSteps, bestChoiceIndex, map, levers, n, trophy, powerSetList, k);

    for (int i = 0; i < n; ++i)
        delete[] map[i];
    delete[] map;
    delete[] levers;
    delete[] powerSetList;
    return 0;
}